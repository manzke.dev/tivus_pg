DO
$$
DECLARE r record;
BEGIN
FOR r IN (SELECT table_name, constraint_name
          FROM information_schema.table_constraints
          WHERE table_schema = 'public' AND constraint_type = 'FOREIGN KEY' ) loop
    RAISE info '%','dropping ' || r.constraint_name;
    EXECUTE CONCAT('ALTER TABLE public.', r.table_name  ,' DROP CONSTRAINT ', r.constraint_name);
END loop;
END;
$$
;

DROP TABLE IF EXISTS public.conta CASCADE;
CREATE TABLE IF NOT EXISTS public.conta (
    id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v1(),
    dominio VARCHAR(128),
    dominio_usuario VARCHAR(128),
    dominio_senha VARCHAR(128)
);

DROP TABLE IF EXISTS public.config CASCADE;
CREATE TABLE IF NOT EXISTS public.config (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(256) NOT NULL,
    valor VARCHAR(256) NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS config_nome_udx ON config(nome);

DROP TABLE IF EXISTS public.agente CASCADE;
CREATE TABLE IF NOT EXISTS public.agente (
    id SERIAL PRIMARY KEY,
    conta_id UUID REFERENCES conta(id) ON DELETE CASCADE,
    nome VARCHAR(128) NOT NULL,
    is64bits BOOL DEFAULT TRUE,
    username VARCHAR(64),
    arquitetura INT NOT NULL DEFAULT 64
);

DROP TABLE IF EXISTS public.aplicativo CASCADE;
CREATE TABLE IF NOT EXISTS public.aplicativo (
    id SERIAL PRIMARY KEY,
    conta_id UUID NOT NULL REFERENCES conta(id) ON DELETE CASCADE,
    nome VARCHAR NOT NULL,
    comando VARCHAR NOT NULL,
    argumentos VARCHAR,
    versao VARCHAR NOT NULL DEFAULT '1.0',
    exit_code VARCHAR NOT NULL DEFAULT '0',
    forcar BOOL DEFAULT FALSE,
    descricao VARCHAR,
    habilitado BOOL DEFAULT TRUE,
    usuario BOOL DEFAULT FALSE,
    process_name VARCHAR,
    arquitetura INT DEFAULT 64
);

DROP TABLE IF EXISTS public.agente_aplicativo CASCADE;
CREATE TABLE IF NOT EXISTS public.agente_aplicativo (
    id SERIAL PRIMARY KEY,
    agente_id INT NOT NULL REFERENCES agente(id) ON DELETE CASCADE,
    aplicativo_id INT NOT NULL REFERENCES aplicativo(id) ON DELETE CASCADE,
    versao VARCHAR,
    data TIMESTAMP
);

DROP TABLE IF EXISTS public.usuario CASCADE;
CREATE TABLE IF NOT EXISTS public.usuario (
    id SERIAL PRIMARY KEY,
    conta_id UUID NOT NULL REFERENCES conta(id) ON DELETE CASCADE,
    username VARCHAR NOT NULL
    );

DROP TABLE IF EXISTS public.usuario_aplicativo CASCADE;
CREATE TABLE IF NOT EXISTS public.usuario_aplicativo (
    id SERIAL PRIMARY KEY,
    usuario_id INT NOT NULL REFERENCES usuario(id) ON DELETE CASCADE,
    aplicativo_id INT NOT NULL REFERENCES aplicativo(id) ON DELETE CASCADE,
    versao VARCHAR,
    data TIMESTAMP
    );

DROP TABLE IF EXISTS public.grupo CASCADE;
CREATE TABLE IF NOT EXISTS public.grupo (
    id SERIAL PRIMARY KEY,
    conta_id UUID NOT NULL REFERENCES conta(id) ON DELETE CASCADE,
    nome VARCHAR NOT NULL,
    auto_agente BOOL DEFAULT FALSE
);

DROP TABLE IF EXISTS public.grupo_agente CASCADE;
CREATE TABLE IF NOT EXISTS public.grupo_agente (
    id SERIAL PRIMARY KEY,
    grupo_id INT NOT NULL REFERENCES grupo(id) ON DELETE CASCADE,
    agente_id INT NOT NULL REFERENCES agente(id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS public.grupo_aplicativo CASCADE;
CREATE TABLE IF NOT EXISTS public.grupo_aplicativo (
    id SERIAL PRIMARY KEY,
    grupo_id INT NOT NULL REFERENCES grupo(id) ON DELETE CASCADE,
    aplicativo_id INT NOT NULL REFERENCES aplicativo(id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS public.grupo_usuario CASCADE;
CREATE TABLE IF NOT EXISTS public.grupo_usuario (
    id SERIAL PRIMARY KEY,
    grupo_id INT NOT NULL REFERENCES grupo(id) ON DELETE CASCADE,
    usuario_id INT NOT NULL REFERENCES usuario(id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS public.historico CASCADE;
CREATE TABLE IF NOT EXISTS public.historico (
    id SERIAL PRIMARY KEY,
    agente_id INT  REFERENCES agente(id) ON DELETE CASCADE,
    usuario_id INT  REFERENCES usuario(id) ON DELETE CASCADE,
    data TIMESTAMP,
    texto TEXT,
    tipo INT NOT NULL DEFAULT 0
);

DROP VIEW IF EXISTS public.agente_aplicativo_view;
CREATE VIEW public.agente_aplicativo_view AS
	SELECT AA.id, AA.agente_id, AA.aplicativo_id, AA.versao as versao_instalada, A.nome, A.versao,
	       agente.nome as agente_nome, AA.data
    FROM agente_aplicativo AA
    JOIN aplicativo A on A.id = AA.aplicativo_id
    JOIN agente on agente.id = AA.agente_id;

DROP VIEW IF EXISTS public.aplicativo_view;
CREATE VIEW public.aplicativo_view	AS
    SELECT DISTINCT  GA.agente_id, 0 as usuario_id,
	                 A.nome, A.id, A.argumentos, A.comando, A.exit_code, A.forcar, A.conta_id,
	                 A.versao, A.descricao, A.process_name,
                     COALESCE( AA.versao, '0') as versao_instalada , A.usuario
    FROM grupo_agente GA
    JOIN grupo_aplicativo GAP ON GAP.grupo_id = GA.grupo_id
    JOIN aplicativo A ON A.id = GAP.aplicativo_id and A.habilitado = TRUE AND A.usuario = FALSE
    LEFT JOIN agente_aplicativo AA on AA.aplicativo_id = GAP.aplicativo_id AND AA.agente_id = GA.agente_id
    UNION
    SELECT DISTINCT 0 AS agente_id,  GU.usuario_id,
                    A.nome, A.id, A.argumentos, A.comando, A.exit_code, A.forcar, A.conta_id, A.versao, A.descricao, A.process_name,
                    COALESCE( UA.Versao, '0') as versao_instalada , A.usuario
    FROM grupo_usuario GU
    JOIN grupo_aplicativo GAP ON GAP.grupo_id = GU.grupo_id
    JOIN aplicativo A ON A.id = GAP.aplicativo_id and A.habilitado = TRUE AND A.usuario = TRUE
    LEFT JOIN usuario_aplicativo UA on UA.aplicativo_id = GAP.aplicativo_id AND UA.usuario_id = GU.usuario_id;

DROP VIEW IF EXISTS public.historico_view;
CREATE VIEW public.historico_view	AS
    SELECT H.id, H.agente_id, H.data, H.texto, A.nome
    FROM historico H
    LEFT JOIN agente A ON A.id = H.agente_id;

DROP VIEW IF EXISTS public.usuario_aplicativo_view;
CREATE VIEW public.usuario_aplicativo_view AS
	SELECT UA.id, UA.usuario_id, UA.aplicativo_id, UA.versao as versao_instalada, A.nome, A.versao,
	U.username , UA.data
FROM usuario_aplicativo UA
JOIN aplicativo A on A.id = UA.aplicativo_id
JOIN usuario U on U.id = UA.usuario_id;

